<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;

class penulis extends Model
{
    protected $table = 'penulis';
    protected $fillable = ['id','nama','notelp','email','alamat'];

public function buku(){
		return $this->belongsToMany(Buku::class);
	}
}