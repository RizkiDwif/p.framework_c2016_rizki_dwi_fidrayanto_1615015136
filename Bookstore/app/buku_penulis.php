<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\penulis;
use App\buku;

class buku_penulis extends Model
{
    protected $table 'buku_penulis';
    protected $fillable = ['id','penulis_id'];

     public function penulis(){
    	return $this->belongsToMany(penulis::class);
    }

    public function buku(){
    	return $this->belongsToMany(buku::class);
    }
}
