<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pembeli;
use App\Buku;

class buku_pembeli extends Model
{
    protected $table = 'buku_pembeli';
    protected $fillable = ['id','pembeli_id','buku_id'];

     public function pembeli(){
    	return $this->belongsToMany(Pembeli::class);
    }

    public function buku(){
    	return $this->belongsToMany(Buku::class);
    }
}