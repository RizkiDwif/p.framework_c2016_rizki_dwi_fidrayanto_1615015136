<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pengguna;
use App\buku;

class pembeli extends Model
{
    protected $table = 'pembeli';
    protected $fillable = ['id','nama','notlp','email','alamat'];

public function Pengguna(){
		return $this->belongsTo(Pengguna::class);
	}

public function buku(){
	    return $this->belongsToMany(buku::class);
     }
}
