<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pembeli;
use App\Admin;
class Pengguna extends Model
{
    protected $table = 'pengguna';
    protected $fillable = ['nama','password'];
    protected $hidden = ['password','remember_token'];

public function Pembeli(){
		return $this->hasOne(Pembeli::class);
	}

public function admin(){
    	return $this->hasOne(Admin::class);
    }
}