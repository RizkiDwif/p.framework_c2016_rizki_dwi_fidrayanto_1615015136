<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $table = 'admin';
    protected $fillable = ['id','nama','notlp','email','alamat','pengguna_id'];

    public function pengguna(){
    	return $this->belongsTo(pengguna::class);
    }
}
