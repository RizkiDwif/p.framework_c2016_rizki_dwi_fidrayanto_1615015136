@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Buku
		<div class="pull-right">
			Tambah Data <a href="{{ url('tambah/buku')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Judul Buku</td>
					<td>Penerbit</td>
					<td>Tanggal</td>
					<td>Kategori id</td>
				</tr>
				@foreach($buku as $Buku)
					
				<tr>
					<td >{{ $Buku->judul }}</td>
					<td >{{ $Buku->penerbit}}</td>
					<td >{{ $Buku->tanggal }}</td>
					<td >{{ $Buku->kategori->deskripsi }}</td>

					<!--
					<td >
					<a href="{{url('buku/edit/'.$Buku->id)}}"><img src="{{ asset('icon/edit.png') }}" height="20"></img>Edit</a>
					<a href="{{url('buku/hapus/'.$Buku->id)}}"><img src="{{ asset('icon/hapus.png') }}" height="20"></img>Hapus</a>
					</td>-->
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

